const Plugin = requirePlugin("myPlugin");

Page({
  data: {
    reachBottomNum: 0,
    pullDownRefreshNum: 0,
    userInfo: '',
    scrollTop: 0,
    autoScrollTop: -1,
  },
  onLoad(options) {
    // const userInfo = {
    //   userType: 0,  //2：特邀专家  0:普通用户
    //   unid: '123123',
    //   openid: 'of1Di5PPzna0fpDTdQGpA3qNKfLE',
    //   nick_name: '仅此而已2',
    //   avatarUrl: 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLRSpGlBNkCDtSMldIbS4yaPVAMD8Rx5zKiarkMzXQgmdksibib05Mz0wnKAZwwq519alUeJibf5lSzmg/132',
    //   bbs_no:0,
    //     goodd_no:0
    // };
    var __self = this;
      getApp().userInfoGotten = function(){
          __self.setData({ userInfo: wx.getStorageSync('userInfo') });
      }
      if (wx.getStorageSync('userInfo').length>10){
        this.setData({ userInfo: wx.getStorageSync('userInfo') });
      }




    // 注意一定要先传递好用户信息，然后在处理下面两段跳转逻辑
    // 用来跳转到绑定官方帐号的界面
    if (options.official && options.official == 'wx') {
      wx.navigateTo({url: Plugin.navigator.authorization});
    }

    //用来处理跳转到帖子详情页
    if (options.path && options.path == 'detail' && options.id) {
      wx.navigateTo({url: `${Plugin.navigator.detail}?id=${options.id}`});
    }
  },
  onReachBottom() {
    this.setData({reachBottomNum: ++this.data.reachBottomNum});
  },
  onPullDownRefresh() {
    this.setData({pullDownRefreshNum: ++this.data.pullDownRefreshNum});
    wx.hideNavigationBarLoading();
    wx.stopPullDownRefresh();
  },
  onPageScroll({scrollTop}){
    clearTimeout(this.onPageScrollTimer);
    this.onPageScrollTimer = setTimeout(() => {
      this.setData({scrollTop});
    }, 100);
  },
  onPageScrollTimer() {
  },
  onIsscroll({detail}){
    this.setData({autoScrollTop: detail.height});
    const pages = getCurrentPages();
    const route = pages[pages.length-1] && pages[pages.length-1].route || '';
    //到时候这个判断写社区的地址，例：/page/community/community_index/community_index
    if(route && route == 'pages/index/index'){
      this.onShow();
    }
  },
  onShow() {
    const autoScrollTop = this.data.autoScrollTop;
    if (autoScrollTop < 0) return;
    this.setData({autoScrollTop: 0});
    wx.pageScrollTo({scrollTop: autoScrollTop, duration: 300});
  },
  onShareAppMessage () {
    return {
      title: '分享的标题',
      path: '/page/community/community_index/community_index',
      imageUrl: '',
    }
  },
  
});
