Page({
  data: {
    reachBottomNum: 0,
    userInfo: '',
    nav: ['课程', '帖子'],
    navIndex: 0,
  },
  onLoad() {
    // const userInfo = {
    //   userType: 0,  //2：特邀专家  0:普通用户
    //   unid: '123123',
    //   openid: 'of1Di5PPzna0fpDTdQGpA3qNKfLE',
    //   nick_name: '仅此而已2',
    //   avatarUrl: 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLRSpGlBNkCDtSMldIbS4yaPVAMD8Rx5zKiarkMzXQgmdksibib05Mz0wnKAZwwq519alUeJibf5lSzmg/132',
    // };
      this.setData({ userInfo: wx.getStorageSync('userInfo') });
  },
  onReachBottom() {
    this.setData({ reachBottomNum: ++this.data.reachBottomNum });
  },
  switch(e) {
    const navIndex = e.currentTarget.dataset.index;
    this.setData({ navIndex });
  }
});
