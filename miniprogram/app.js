
App({
  data: {
  },
  onShow() {



  },
  onLaunch() {
      const Plugin = requirePlugin("myPlugin");
      
      Plugin.util.setAppKey(this.globalData.app_key)
      var that = this;
      console.log(this.buildUrl('user/getOpenid'))
      wx.login({
          success: res2 => {

                wx.request({
                    url: this.buildUrl('user/getOpenid'),
                    data:{
                        code:res2.code
                    },
                    success: res => {
                        console.log(res)
                        that.globalData.openid = res.data.openid;
                        wx.setStorageSync('openid', res.data.openid)
                        wx.getSetting({
                            success: function (res) {
                   
                                if (getApp().userInfoGotten) {
                                    getApp().userInfoGotten()
                                }
                                
                                
                                
                                
                                
                                
                                
                                
                                             if (res.authSetting['scope.userInfo']) {
                                    wx.getUserInfo({
                                        success: function (res) {
                                            //从数据库获取用户信息
                                            that.queryUsreInfo();
                                            //用户已经授权过
                                            // wx.switchTab({
                                            //     url: ''
                                            // })
                                        }
                                    });
                                }
                            }
                        })
                    }
                })
             
          }
      });

        
      var userInfo = wx.getStorageSync('userInfo');
      if (!userInfo) {
          wx.redirectTo({
              url: '/pages/authorization/authorization',
          })
      }
  },
  buildUrl:function(url){
      return this.globalData.wx_url+this.globalData.app_key+'/'+url
  },
    globalData: {
        openid: '',
        app_key:'weixin',
        wx_url: 'https://forum-saas.hujingtech.com/api/',
    },
    queryUsreInfo: function () {
        wx.request({
            url: this.buildUrl('user/getUserInfo'),
            data: {
                openid: this.globalData.openid
            },
            header: {
                'content-type': 'application/json'
            },
            success: function (res) {
                console.log(res.data.content);
                getApp().globalData.userInfo = res.data.content;
                wx.setStorageSync('userInfo', JSON.stringify(res.data.content))

                if(getApp().userInfoGotten){
                    getApp().userInfoGotten()
                }






            }
        })
    },
});
