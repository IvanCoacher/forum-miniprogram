// const appId = 'wx1e96d9b02b888f48';  //虎鲸
const appId = 'wx6a518df03a0357c7';
const domain = `plugin-private://${appId}/pages/`;
const https = 'https://forum-saas.hujingtech.com/api/';
var app_key = '';
const navigatorVersion = 'release';  // develop（开发版）、trial（体验版）、release（正式版）
const communityPath = '/page/community/community_index/community_index';

function ajax(options = {}) {

  if (!options.url) return;
  options.url = `${https}${app_key}/${options.url}`;
  options.method = options.type || 'POST';
  options.data = options.data || {};
  options.header = {
    // 'content-type': 'application/json',
    'content-type': 'application/x-www-form-urlencoded',
  };
  if (options.success) {
    const success = options.success;
    options.success = ({ data }) => {
      success(data);
    };
  }
  wx.request(options);
}

function setAppKey(options){
    app_key = options || 'weixin';
}
function getAppKey(){
    return app_key;
}

function getDateDiff(dateTimeStamp) {
  dateTimeStamp = Date.parse(dateTimeStamp.replace(/-/gi, "/"));
  const minute = 1000 * 60;
  const hour = minute * 60;
  const day = hour * 24;
  const halfamonth = day * 15;
  const month = day * 30;
  const now = new Date().getTime();
  const diffValue = now - dateTimeStamp;
  if (diffValue < 0) {
    return;
  }
  const monthC = diffValue / month;
  const weekC = diffValue / (7 * day);
  const dayC = diffValue / day;
  const hourC = diffValue / hour;
  const minC = diffValue / minute;
  let result = '';
  if (monthC >= 1) {
    result = `${parseInt(monthC)}月前`;
  }
  else if (weekC >= 1) {
    result = `${parseInt(weekC)}周前`;
  }
  else if (dayC >= 1) {
    result = `${parseInt(dayC)}天前`;
  }
  else if (hourC >= 1) {
    result = `${parseInt(hourC)}小时前`;
  }
  else if (minC >= 1) {
    result = `${parseInt(minC)}分钟前`;
  } else {
    result = `刚刚`;
  }
  return result;
}

const Event = {
  on(name, fn) {
    const eventData = Event.data;
    if (eventData.hasOwnProperty(name)) {
      eventData[name].push(fn);
    } else {
      eventData[name] = [fn];
    }
    return this;
  },
  fire(name, data, thisArg) {
    const fnList = Event.data[name];
    const len = fnList.length;
    let fn;
    if (!fnList.length) {
      throw new Error(`Cannot find broadcast event ${name}`);
    }
    for (let i = 0; i < len; i++) {
      fn = fnList[i];
      fn.apply(thisArg, [data, name]);
    }
    return this;
  },
  data: {},
};

module.exports = {
  ajax,
  appId,
  domain,
  https,
  navigatorVersion,
  communityPath,
  Event,
  getDateDiff,
  setAppKey,
  app_key,
  getAppKey
};
