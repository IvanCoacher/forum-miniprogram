import { ajax, domain, Event } from '../../utils/util';

Page({
  data: {
    domain,
    content: '',
    id: '',
  },
  onLoad(options) {
    wx.setNavigationBarTitle({ title: '添加评论' });
    this.setData({ id: options.id });
    if (!this.data.id) {
      wx.navigateBack({ delta: 1 });
    }
  },
  bindTextArea({ detail }) {
    this.setData({ content: detail.value });
  },
  send() {
    if (!this.data.content) {
      wx.showToast({ title: '内容不能为空', icon: 'none', duration: 2000 });
      return;
    }
    this.setData({ isSend: true });
    wx.showLoading({ title: '保存中' });
    const userInfo = JSON.parse(wx.getStorageSync('userInfo'));
    ajax({
      url: 'comment/store',
      data: {
        content: this.data.content,
        openid: userInfo.openid,
        nick_name: userInfo.nick_name,
        head_url: userInfo.avatarUrl,
        expert: userInfo.userType,
        bbs_id: this.data.id,
        level: 1,
      },
      success: ({ code, msg }) => {
        this.setData({ isSend: false });
        if (code != 200) {
          wx.showToast({ title: msg, icon: 'none', duration: 1000 });
        } else {
          wx.showToast({ title: msg, icon: 'success', duration: 1000 });
          Event.fire('UPDATEDETAIL', { id: this.data.id });
          setTimeout(() => wx.navigateBack({ delta: 1 }), 1000);
        }
      }
    });
  },
});
