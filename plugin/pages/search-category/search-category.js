import { ajax, domain } from '../../utils/util';

const defaultData = {
  result: [],
  bbsId: 0,
  resultGet: true,
  ajaxIng: false,
};

Page({
  data: {
    domain,
    value: '',
    type: 0,
    resultCategory: ['社区问答', '技术文章', '案例分享'],
  },
  onLoad(options) {
    wx.setNavigationBarTitle({ title: '搜索内容' });
    this.setData({ value: options.value, type: options.type });
    this.init();
  },
  cancel() {
    wx.navigateBack({ delta: 1 });
  },
  bindConfirm(e) {
    this.setData({ value: e.detail.value });
    this.init();
  },
  getResult(value) {
    if (!value) return;
    this.setData({ value: value, ajaxIng: true });
    ajax({
      url: 'searchDetail.php',
      data: {
        search_key: value,
        forum_id: Number(this.data.type + 1),
        bbs_id: this.data.bbsId,
        handleType: 'next',
        range: 30,
      },
      success: ({ item = [] }) => {
        this.setData({
          ajaxIng: false,
          result: this.data.result.concat(item),
          bbsId: (item.length > 0 && item[item.length - 1].id || 0),
          resultGet: (item.length < 30 ? false : true)
        });
      }
    });
  },
  onReachBottom() {
    if (this.data.ajaxIng || !this.data.resultGet) return;
    this.getResult(this.data.value);
  },
  init() {
    this.setData({ ...defaultData });
    this.getResult(this.data.value);
  },
});