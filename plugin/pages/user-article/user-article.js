import {ajax, domain} from '../../utils/util';

Page({
  data: {
    domain,
    openid: '',
    type: 0,
    tabItem: ['我的发帖', '我的回帖', '精华帖', '我的收藏'],
      urlArr: ['user/userPub', 'user/userReply', 'user/userSuper', 'user/userStore'],
    listData: {},
    userInfo: {},
  },
  onLoad(options) {
    const userInfo = JSON.parse(wx.getStorageSync('userInfo') || '{}');
    const newUserInfo = JSON.parse(options.userInfo || '{}');
    wx.setStorageSync('userInfo', JSON.stringify(Object.assign(userInfo, newUserInfo)));
    this.setData({ type: options.type || 0});
    wx.setNavigationBarTitle({title: this.data.tabItem[this.data.type]});
    this.init();
  },
  getListData() {
    const listData = this.data.listData;
    listData.ajaxIng = true;
    this.setData({listData});
    ajax({
      url: this.data.urlArr[this.data.type],
      data: {
        openid: this.data.userInfo.openid,
        bbs_id: listData.bbsId,
        handleType: 'next',
        range: 20,
      },
      success: ({items = []}) => {
        items.forEach((item) => item.tag = item.tag && item.tag.split(',') || []);
        listData.data = listData.data.concat(items);
        listData.bbsId = items.length > 0 && items[items.length - 1].id || 0;
        listData.dataGet = items.length < 20 ? false : true;
        listData.ajaxIng = false;
        this.setData({listData});
      }
    });
  },
  onReachBottom() {
    if (this.data.listData.ajaxIng || !this.data.listData.dataGet) return;
    this.getListData();
  },
  onPullDownRefresh() {
    this.init();
    wx.hideNavigationBarLoading();
    wx.stopPullDownRefresh();
  },
  init() {
    const userInfo = JSON.parse(wx.getStorageSync('userInfo')|| '{}');
    if (userInfo.openid) {
      this.setData({userInfo, listData: {data: [], dataGet: true, ajaxIng: false, bbsId: 0}});
      this.getListData();
    }
  },
  onMyEvent({detail}){
    this.init();
  },
});
