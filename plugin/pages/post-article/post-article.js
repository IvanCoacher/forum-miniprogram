import {
    ajax,
    https,
    domain,
    Event,
    app_key,
    getAppKey
} from '../../utils/util';

Page({
    data: {
        domain,
        tagItem: [],
        checkTag: [],
        maxlength: 20,
        titleCursor: 0,
        title: '',
        content: '',
        imgs: [],
        imgs2: [],
        imgMaxLen: 9,
        imgsProgress: [],
        isSend: false,
        isUploadImg: false,
    },
    onLoad() {
        wx.setNavigationBarTitle({
            title: '发布问题'
        });
        this.getTagItem();
    },
    getTagItem() {
        ajax({
            url: 'bbs/tagList',
            data: {
                forum_id: 1
            },
            success: ({
                code,
                msg,
                items
            }) => {
                if (code != 200) {
                    wx.showToast({
                        title: msg,
                        icon: 'none',
                        duration: 2000
                    });
                } else {
                    items.forEach((item) => item.isChecked = false);
                    this.setData({
                        tagItem: items
                    });
                }
            }
        });
    },
    chooseTag(e) {
        const tagItem = this.data.tagItem;
        const index = e.currentTarget.dataset.index;
        if (this.data.checkTag.length >= 3 && !tagItem[index].isChecked) {
            wx.showToast({
                title: '最多只能选3个问题标签',
                icon: 'none',
                duration: 1000
            });
            return;
        }
        tagItem[index].isChecked = !tagItem[index].isChecked;
        let checkTag = tagItem.filter((item) => {
            if (item.isChecked) return item;
        });
        checkTag = checkTag.map((item) => item.id);
        this.setData({
            tagItem,
            checkTag
        });
    },
    bindInput({
        detail
    }) {
        this.setData({
            title: detail.value,
            titleCursor: detail.cursor
        });
    },
    bindTextArea({
        detail
    }) {
        this.setData({
            content: detail.value
        });
    },
    uploadImg() {
        const oldimgsLen = this.data.imgs.length;
        const count = this.data.imgMaxLen - oldimgsLen;
        if (count <= 0) {
            wx.showToast({
                title: `最多上传${this.data.imgMaxLen}张`,
                icon: 'none',
                duration: 1500
            });
            return;
        }
        wx.chooseImage({
            count,
            success: ({
                tempFilePaths
            }) => {
                this.setData({
                    isUploadImg: true
                });
                const _this = this;
                let imgsProgress = this.data.imgsProgress.concat(tempFilePaths.map((item) => 0));
                let imgs = this.data.imgs.concat(tempFilePaths);
                let imgs2 = this.data.imgs2.concat(tempFilePaths);
                this.setData({
                    imgs,
                    imgsProgress,
                    imgs2
                });
                let uploadTask = {};
                let index = oldimgsLen;
                xx(tempFilePaths[index - oldimgsLen]);

                function xx(item) {
                    console.log(getAppKey())
                    uploadTask[index] = wx.uploadFile({
                        url: `${https}${getAppKey()}/system/imgUpload`,
                        filePath: item,
                        name: 'photo',
                        success: ({
                            data
                        }) => {
                            data = JSON.parse(data);
                            imgs[index] = data.imgUrl;
                            _this.setData({
                                imgs
                            });
                            index++;
                            if (index < imgs.length) {
                                xx(tempFilePaths[index - oldimgsLen]);
                            } else {
                                _this.setData({
                                    isUploadImg: false
                                });
                            }
                        },
                    });
                    uploadTask[index].onProgressUpdate(({
                        progress
                    }) => {
                        imgsProgress[index] = progress;
                        _this.setData({
                            imgsProgress
                        });
                    })
                }
            },
        });
    },
    send() {
        if (this.data.isUploadImg) {
            wx.showToast({
                title: '图片上传中',
                icon: 'none',
                duration: 1000
            });
            return;
        }
        if (!this.data.title) {
            wx.showToast({
                title: '标题不能为空',
                icon: 'none',
                duration: 1000
            });
            return;
        }
        if (!this.data.content) {
            wx.showToast({
                title: '内容不能为空',
                icon: 'none',
                duration: 1000
            });
            return;
        }
        this.setData({
            isSend: true
        });
        wx.showLoading({
            title: '保存中'
        });
        const userInfo = JSON.parse(wx.getStorageSync('userInfo'));
        console.log('userInfo')
        console.log(userInfo)
        const checkTag = this.data.checkTag;
        let checkTagStr = [];
        checkTag.forEach((item) => {
            this.data.tagItem.forEach((item2) => {
                if (item2.id == item) {
                    checkTagStr.push(item2.tname);
                }
            });
        });
        ajax({
            url: 'bbs/store',
            data: {
                title: this.data.title,
                content: this.data.content,
                tag: checkTag.join(','),
                tag_content: checkTagStr.join(','),
                openid: userInfo.openid,
                nick_name: userInfo.nick_name,
                head_url: userInfo.avatarUrl,
                expert: userInfo.userType,
                forum_id: '1',
                img_list: this.data.imgs.join('|'),
            },
            success: ({
                code,
                msg,
                bbs_id = ''
            }) => {
                this.setData({
                    isSend: false
                });
                if (code != 200) {
                    wx.showToast({
                        title: msg,
                        icon: 'none',
                        duration: 1000
                    });
                } else {
                    wx.showToast({
                        title: msg,
                        icon: 'success',
                        duration: 1000
                    });
                    Event.fire('UPDATEINDEX', {
                        update: true,
                        bbsId: bbs_id
                    });
                    setTimeout(() => wx.navigateBack({
                        delta: 1
                    }), 1000);
                }
            }
        });
    },
    delImg(e) {
        let error = 0;
        this.data.imgsProgress.forEach((item) => {
            if (item < 100) error++;
        });
        if (error > 0) {
            wx.showToast({
                title: `图片上传中，请稍后再试`,
                icon: 'none',
                duration: 2000
            });
            return;
        }
        const index = e.currentTarget.dataset.index;
        this.data.imgs.splice(index, 1);
        this.data.imgs2.splice(index, 1);
        this.data.imgsProgress.splice(index, 1);
        this.setData({
            imgs: this.data.imgs,
            imgs2: this.data.imgs2,
            imgsProgress: this.data.imgsProgress
        });
    },
});