import {ajax, domain} from '../../utils/util';

Page({
  data: {
    domain,
    value: '',
    result: [],
    noData: false,
    resultCategory: ['社区问答', '技术文章', '案例分享'],
  },
  onLoad() {
    wx.setNavigationBarTitle({title: '搜索内容'});
  },
  cancel(){
    wx.navigateBack({delta: 1});
  },
  bindConfirm(e) {
    this.getResult(e.detail.value);
  },
  getResult(value){
    if (!value) return;
    this.setData({value: value});
    ajax({
      url: 'bbs/search',
      data: {search_key: value},
      success: ({code, item1=[], item2=[], item3=[]}) => {
        if (code != 200) {
          this.setData({ noData: true });
        } else {
          this.setData({
            result: [item1, item2, item3],
            noData: (item1.length <= 0 && item2.length <= 0 && item3.length <= 0 ? true : false)
          });
        }
      }
    });
  },
});
