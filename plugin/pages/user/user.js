import {ajax, domain} from '../../utils/util';

const defaultData = JSON.stringify({ data: [], dataGet: true, ajaxIng: false, page: 1 });

Page({
  data: {
    domain,
    userId: '',
    userInfo: {},
    tabIndex: 0,
    tabItem: ['社区问答', '技术文章', '案例分享'],
    articleInfo: {},
  },
  onLoad(options) {
    wx.setNavigationBarTitle({ title: '用户详情' });
    this.setData({ userId: options.userId });
    if (!this.data.userId) {
      wx.navigateBack({ delta: 1 });
    } else {
      this.init();
    }
  },
  getarticleData() {
    const tabIndex = this.data.tabIndex;
    const articleInfo = this.data.articleInfo;
    if(articleInfo[tabIndex].ajaxIng) return;
    if(!articleInfo[tabIndex].dataGet) return;
    articleInfo[tabIndex].ajaxIng = true;
    this.setData({ articleInfo });
    ajax({
      url: 'user/userShowList',
      data: {
        forum_id: (this.data.tabIndex + 1),
        user_id: this.data.userId,
        page: articleInfo[tabIndex].page,
        range: 20,
      },
      success: ({ items = [] }) => {
        items.forEach((item) => item.tag = item.tag  || []);
        items.map((item) => {
          item._udate = item.udate;
          return item;
        });

        articleInfo[tabIndex].ajaxIng = false;
        articleInfo[tabIndex].data = articleInfo[tabIndex].data.concat(items);
        articleInfo[tabIndex].dataGet = items.length < 20 ? false : true;
        articleInfo[tabIndex].page++;
        this.setData({ articleInfo });
      }
    });
  },
  switchTab(e) {
    const index = e.currentTarget.dataset.index;
    this.setData({ tabIndex: index });
    // const articleInfo = this.data.articleInfo;
    // articleInfo[index] = JSON.parse(defaultData);
    // this.setData({ tabIndex: index, articleInfo });
    this.getarticleData();
  },
  getUserInfo() {
    ajax({
      url: 'user/userShow',
      data: { user_id: this.data.userId },
      success: (result) => {
        if (result.code != 200) {
          wx.showToast({ title: result.msg, icon: 'none', duration: 2000 });
        } else {
          this.setData({ userInfo: result.content });
        }
      }
    });
  },
  init() {
    const articleInfo = {};
    this.data.tabItem.forEach((item, index) => articleInfo[index] = JSON.parse(defaultData));
    this.setData({ articleInfo });
    this.getarticleData();
    this.getUserInfo();
  },
  onReachBottom() {
    this.getarticleData();
  },
});
