import {ajax, Event} from '../../utils/util';

Page({
  data: {
    userInfo: {},
  },
  onLoad() {
    this.shouquan();
  },
  onMyEvent({}){
    this.shouquan();
  },
  shouquan(){
    const userInfo = JSON.parse(wx.getStorageSync('userInfo') || '{}');
    if (!userInfo.openid) return;
    wx.showLoading({ title: '官方帐号授权中...' });
    ajax({
      url: 'addForum3.php',
      data: {
        openid: userInfo.openid,
        nick_name: userInfo.nick_name,
        head_url: userInfo.avatarUrl,
      },
      success: () => {
        wx.hideLoading();
        wx.showToast({ title: '授权成功', icon: 'success', duration: 1000 });
        userInfo.userType = 1;
        wx.setStorageSync('userInfo', JSON.stringify(userInfo));
        setTimeout(() => wx.navigateBack({delta: 1}), 1200);
      }
    });
  },
});
