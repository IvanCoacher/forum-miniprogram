import {
    ajax,
    domain,
    Event,
    getDateDiff,
    communityPath
} from '../../utils/util';

const WxParse = require('../../wxParse/wxParse.js');

const commentDefaultData = JSON.stringify({
    data: [],
    ajax: false,
    isGet: true,
    comment_id: 0,
});

Page({
    data: {
        domain,
        id: '',
        tabItem: ['社区问答', '技术文章', '案例分享'],
        articleInfo: {},
        imgList: [],
        isGetArticleInfo: false,
        goComment: false,
        userInfo: {},
        commentData: {},
        time: '',
    },
    onLoad(options) {
        wx.setNavigationBarTitle({
            title: ''
        });
        this.setData({
            id: options.id,
            goComment: options.goComment || false
        });
        if (!this.data.id) {
            wx.navigateBack({
                delta: 1
            });
        }
        this.init();
        Event.on('UPDATEDETAIL', ({
            id
        }) => {
            if (id == this.data.id) {
                this.init();
            }
        });
        Event.on('UPDATEDETAILREPLY', ({
            id,
            commentId
        }) => {
            let iscommentId = -1;
            const commentData = this.data.commentData;
            console.log(commentData)
            if (!commentData) {
                return;
            }
            commentData.data.forEach((item, index) => {
                if (item.comment_id == commentId) {
                    iscommentId = index;
                }
            });
            if (id == this.data.id && iscommentId > -1) {
                commentData.data[iscommentId].isGetMoreReply = true;
                commentData.data[iscommentId].reply = [];
                this.getReply({
                    currentTarget: {
                        dataset: {
                            index: iscommentId
                        }
                    }
                });
            }
        });
    },
    onHide() {
        this.setData({
            time: ''
        });
    },
    getArticleInfo() {
        ajax({
            url: 'bbs/show',
            data: {
                bbs_id: this.data.id,
                openid: this.data.userInfo.openid
            },
            success: (data) => {
                if (data.code != 200) {
                    wx.showToast({
                        title: data.msg,
                        icon: 'none',
                        duration: 2000
                    });
                    setTimeout(() => wx.navigateBack({
                        delta: 1
                    }), 2000);
                } else {
                    var data = data.content
                    data.img_list = data.img_list.filter((item) => {
                        if (item) return item;
                    });
                    const imgList = data.img_list.map(() => {
                        return {
                            width: 0,
                            height: 0
                        };
                    });
                    data._udate = data.udate;
                    data.tag = data.tag || [];
                    this.setData({
                        articleInfo: data,
                        isGetArticleInfo: true,
                        imgList
                    });
                    const article = `${data.content}`;
                    WxParse.wxParse('article', 'html', article, this, 15);

                    wx.setNavigationBarTitle({
                        title: this.data.tabItem[this.data.articleInfo.forum_id - 1]
                    });
                    if (this.data.goComment && this.data.articleInfo.img_list.length <= 0) {
                        this.goDetailComment();
                    }
                }
            }
        });
    },
    getComment() {
        let commentData = this.data.commentData;
        if (!commentData.isGet) return;
        if (commentData.ajax) return;
        commentData.ajax = true;
        this.setData({
            commentData
        });
        ajax({
            url: 'comment/list',
            data: {
                bbs_id: this.data.id,
                openid: this.data.userInfo.openid,
                range: 20,
                comment_id: this.data.commentData.comment_id,
            },
            success: ({
                code,
                comment = []
            }) => {
                commentData.ajax = false;
                if (code != 200) {
                    commentData.isGet = false;
                    this.setData({
                        commentData
                    });
                    return;
                }
                comment = comment.map((item) => {
                    //   item._udate = item;
                    item.reply = item.reply || [];
                    item.isGetMoreReply = item.reply.length > 5 ? true : false;
                    if (item.reply.length > 5) {
                        item.reply.length = 5
                    }
                    return item;
                });
                commentData.data = commentData.data.concat(comment);
                commentData.comment_id = comment[comment.length - 1].comment_id;
                commentData.isGet = comment.length < 20 ? false : true;
                this.setData({
                    commentData
                });
            }
        });
    },
    getReply(e) {
        const index = e.currentTarget.dataset.index;
        let commentData = this.data.commentData;
        const commentInfo = commentData.data[index];
        if (!commentInfo.isGetMoreReply) return;
        ajax({
            url: 'comment/replyList',
            data: {
                bbs_id: this.data.id,
                openid: this.data.userInfo.openid,
                comment_id: commentInfo.comment_id,
                comment_user_id: commentInfo.user_id,
                reply_id: (commentInfo.reply[4] && commentInfo.reply[4].reply_id || 0),
            },
            success: ({
                code,
                reply = []
            }) => {
                commentInfo.isGetMoreReply = false;
                commentInfo.reply = commentInfo.reply.concat(reply);
                this.setData({
                    commentData
                });
            }
        });
    },
    onShareAppMessage() {
        return {
            title: this.data.articleInfo.title,
            path: `${communityPath}?path=detail&id=${this.data.id}`,
        }
    },
    agreeUser(e) {
        let handletype = e.currentTarget.dataset.handletype;
        const index = e.currentTarget.dataset.index;
        const commentData = this.data.commentData;
        const commentInfo = commentData.data[index];


        if (handletype == 1) {
            if (commentInfo.agree == 0 || commentInfo.agree == 2) {
                commentInfo.good = ++commentInfo.good;
                handletype = 1;
            }

            if (commentInfo.agree == 1) {
                commentInfo.good = --commentInfo.good;
                handletype = 0;
            }

            if (commentInfo.agree == 2) {
                commentInfo.bad = --commentInfo.bad;
            }
        }

        if (handletype == 2) {
            if (commentInfo.agree == 0 || commentInfo.agree == 1) {
                commentInfo.bad = ++commentInfo.bad;
                handletype = 2;
            }

            if (commentInfo.agree == 1) {
                commentInfo.good = --commentInfo.good;
            }

            if (commentInfo.agree == 2) {
                commentInfo.bad = --commentInfo.bad;
                handletype = 0;
            }
        }

        commentInfo.agree = handletype;

        this.setData({
            commentData
        });
        ajax({
            url: 'comment/changeAgree',
            data: {
                openid: this.data.userInfo.openid,
                nick_name: this.data.userInfo.nick_name,
                head_url: this.data.userInfo.avatarUrl,
                level: 1,
                comment_id: commentInfo.comment_id,
                author_id: commentInfo.user_id,
                handleType: handletype,
                expert: this.data.userInfo.userType,
                bbs_id: this.data.id,
            },
        });
    },
    changeStore() {
        const articleInfo = this.data.articleInfo;
        const store = Math.abs(articleInfo.isStore - 1);
        articleInfo.isStore = store;
        this.setData({
            articleInfo
        });
        if (store > 0) {
            wx.showToast({
                title: '已收藏',
                icon: 'none',
                duration: 2000
            });
        }
        ajax({
            url: 'bbs/changeStore',
            data: {
                openid: this.data.userInfo.openid,
                nick_name: this.data.userInfo.nick_name,
                head_url: this.data.userInfo.avatarUrl,
                bbs_id: articleInfo.bbs_id,
                forum_id: articleInfo.forum_id,
                author_id: articleInfo.author_id,
                handleType: store,
                expert: this.data.userInfo.userType,
            },
            // success: ({code, msg}) => {
            //   if (code != 200) {
            //     wx.showToast({title: msg, icon: 'none', duration: 1000});
            //   } else {
            //     articleInfo.isStore = store;
            //     this.setData({articleInfo});
            //   }
            // }
        });
    },
    bindload(e) {
        const index = e.currentTarget.dataset.index;
        const detail = e.detail;
        const imgList = this.data.imgList;
        if (detail.width > 345) {
            detail.height = 345 * detail.height / detail.width;
            detail.width = 345;
        }
        imgList[index].width = detail.width * 2;
        imgList[index].height = detail.height * 2;
        this.setData({
            imgList
        });
        if (this.data.goComment && index >= imgList.length - 1) {
            setTimeout(() => this.goDetailComment(), 200);
        }
    },
    previewImage(e) {
        const index = e.currentTarget.dataset.index;
        const imgList = this.data.articleInfo.img_list || [];
        wx.previewImage({
            current: imgList[index],
            urls: imgList
        });
    },
    goDetailComment() {
        wx.createSelectorQuery().in(this).
        select('.content-main').boundingClientRect((res) => {
            wx.pageScrollTo({
                scrollTop: res.height,
                duration: 300
            });
        }).exec()
    },
    init() {
        const userInfo = JSON.parse(wx.getStorageSync('userInfo'));
        if (userInfo.openid) {
            this.setData({
                userInfo,
                commentData: JSON.parse(commentDefaultData)
            });
            this.getArticleInfo();
            this.getComment();
        }
    },
    onMyEvent({
        detail
    }) {
        this.init();
    },
    onReachBottom() {
        this.getComment();
    },
});