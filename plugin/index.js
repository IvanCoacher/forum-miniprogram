import {domain} from './utils/util';

const navigator = {
  // user: `${domain}user/user`,
  userArticle: `${domain}user-article/user-article`,
  authorization: `${domain}authorization/authorization`,
  // search: `${domain}search/search`,
  detail: `${domain}detail/detail`,
};
const util = require('./utils/util');
module.exports = {
  navigator,
  util
};