import {ajax, domain, Event, getDateDiff} from '../../utils/util';

const defaultData = JSON.stringify({data: [], dataGet: true, ajaxIng: false, bbsId: 0, pageNum: 1});

Component({
  properties: {
    reachBottomNum: {
      type: Number,
      value: 0,
      observer() {
        this.getNext();
      }
    },
    pullDownRefreshNum: {
      type: Number,
      value: 0,
      observer() {
        this.init();
      }
    },
    scrollTop: {
      type: Number,
      value: 0,
      observer() {
        this.scrollTop();
      }
    },
    userInfo: {
      type: String,
      value: '{}',
      observer(newVal) {
        const userInfo = JSON.parse(wx.getStorageSync('userInfo') || '{}');
        newVal = JSON.parse(newVal || '{}');
        wx.setStorageSync('userInfo', JSON.stringify(Object.assign(userInfo, newVal)));
      }
    }
  },
  data: {
    domain,
    sliderImgs: [],
    tabIndex: 0,
    tabItem: ['社区问答', '技术文章', '案例分享'],
    listData: {},
    navFixed: false,
  },
  ready() {
    this.init();
    Event.on('UPDATEINDEX', ({update, bbsId}) => {
      if (this.data.tabIndex > 0 && update) return;
      const index = 0;
      const listData = this.data.listData;
      listData[index] = JSON.parse(defaultData);
      this.setData({tabIndex: index, listData});
      this.triggerEvent('isscroll', {height: 0});
      this.getListData({isScroll: true, bbsId: bbsId});
    });
  },
  methods: {
    getSliderImgs() {
      ajax({
        url: 'system/getSlider',
        success: ({code, msg, items}) => {
          if (code != 200) {
            wx.showToast({title: msg, icon: 'none', duration: 2000});
          } else {
            const urlArr = [
              'user/user?userId=2',
              'detail/detail?id=9',
              'search/search',
              'post-article/post-article',
              'comment/comment?id=3',
            ];

            items.forEach((item, index) => {
              item.url = urlArr[index];
            });
            this.setData({sliderImgs: items});
          }
        }
      });
    },
    getListData(options = {}) {
      const tabIndex = this.data.tabIndex;
      const listData = this.data.listData;
      listData[tabIndex].ajaxIng = true;
      this.setData({listData});
      ajax({
        url: 'bbs/list',
        data: {
          forum_id: (this.data.tabIndex + 1),
          bbs_id: listData[tabIndex].bbsId,
          handleType: 'next',
          range: 20,
        },
        success: ({items = []}) => {
          items.forEach((item) => {
            item.tag = item.tag  || [];
          });
          const allData = listData[tabIndex].data.concat(items).map((item) => {
            item._udate = item.udate;
            return item;
          });
          listData[tabIndex].data = allData;
          listData[tabIndex].bbsId = items.length > 0 && items[items.length - 1].id || 0;
          listData[tabIndex].dataGet = items.length < 20 ? false : true;
          listData[tabIndex].pageNum = ++listData[tabIndex].pageNum;
          listData[tabIndex].ajaxIng = false;
          this.setData({listData});
          if (tabIndex == 0 && options.isScroll && listData[tabIndex].pageNum < 4) {
            this.autoScroll(options);
          }
        }
      });
    },
    switchTab(e) {
      const index = e.currentTarget.dataset.index;
      const listData = this.data.listData;
      listData[index] = JSON.parse(defaultData);
      this.setData({tabIndex: index, listData});
      this.getListData();
    },
    getNext(options = {}) {
      const tabIndex = this.data.tabIndex;
      const listData = this.data.listData;
      if (listData[tabIndex].ajaxIng || !listData[tabIndex].dataGet) return;
      this.getListData(options);
    },
    init() {
      const listData = {};
      this.data.tabItem.forEach((item, index) => listData[index] = JSON.parse(defaultData));
      this.setData({listData});
      this.getSliderImgs();
      this.getListData();
    },
    scrollTop() {
      const scrollTop = this.data.scrollTop;
      this.getTopMainHeight((topMainHeight) => {
        this.setData({navFixed: scrollTop >= topMainHeight + 40 ? true : false});
      })
    },
    getTopMainHeight(callback) {
      wx.createSelectorQuery().in(this).select('.top-main').boundingClientRect((res) => {
        if (callback) callback(res.height);
      }).exec()
    },
    autoScroll(options = {}) {
      if (!options.bbsId) return;
      let scrollIndex = -1;
      this.data.listData[this.data.tabIndex].data.forEach((item, index) => {
        if (item.id == options.bbsId) scrollIndex = index;
      });
      if (scrollIndex < 0) {
        this.getNext(options);
      } else {
        wx.createSelectorQuery().in(this).select(`.article-item-${scrollIndex}`).boundingClientRect((res) => {
          this.triggerEvent('isscroll', {height: scrollIndex > 0 ? (res.top - 80) : res.top});
        }).exec();
      }
    },
  }
});