import { ajax, navigatorVersion } from '../../utils/util';

Component({
  properties: {},
  data: {
    isLogin: true,
    navigatorVersion: navigatorVersion,
  },
  ready() {
    const userInfo = JSON.parse(wx.getStorageSync('userInfo') || '{}');
    // this.setData({ isLogin: (userInfo.openid && userInfo.nick_name && userInfo.avatarUrl) ? true : false });
  },
  methods: {
    loginSuccess({ detail }) {
      console.log(detail);
      const userInfo = JSON.parse(wx.getStorageSync('userInfo') || '{}');
      const newUserInfo = {
        openid: '',
        nick_name: detail.userInfo.nickName,
        avatarUrl: detail.userInfo.avatarUrl,
      };
      ajax({
        url: 'getOpenid.php',
        data: {
          code: detail.code,
        },
        success: ({ openid = '', errmsg }) => {
          newUserInfo.openid = openid;
          wx.setStorageSync('userInfo', JSON.stringify(Object.assign(userInfo, newUserInfo)));
          this.setData({ isLogin: true });
          this.triggerEvent('myevent', { login: true })
        }
      });
    },
    bindfail(data){
      console.log(data);
    },
    close() {
      return;
      const userInfo = JSON.parse(wx.getStorageSync('userInfo') || '{}');
    //   const newUserInfo = {
    //     openid: 'of1Di5PPzna0fpDTdQGpA3qNKfLE',
    //     nick_name: '仅此而已2',
    //     avatarUrl: 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLRSpGlBNkCDtSMldIbS4yaPVAMD8Rx5zKiarkMzXQgmdksibib05Mz0wnKAZwwq519alUeJibf5lSzmg/132',
    //   };
    //   wx.setStorageSync('userInfo', JSON.stringify(Object.assign(userInfo, newUserInfo)));
      this.setData({ isLogin: true });
      this.triggerEvent('myevent', { login: true })
    }
  }
});