import {ajax, domain} from '../../utils/util';

Component({
  properties: {
  },
  data: {
  },
  attached() {
    this.shouquan();
  },
  methods: {
    onMyEvent({}){
      this.shouquan();
    },
    shouquan(){
      const userInfo = JSON.parse(wx.getStorageSync('userInfo') || '{}');
      if (!userInfo.openid) return;
      wx.showLoading({ title: '官方帐号授权中...' });
      ajax({
        url: 'getOpenid',
        data: {
          openid: userInfo.openid,
        },
        success: ({ openid }) => {
          wx.hideLoading();
          wx.showToast({ title: '授权成功', icon: 'success', duration: 1000 });
          userInfo.userType = 1;
        //   wx.setStorageSync('userInfo', JSON.stringify(userInfo));
          this.triggerEvent('authorizationevent', {authorization: true})
        }
      });
    },
  }
});