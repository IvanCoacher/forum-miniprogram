import {ajax, domain} from '../../utils/util';

Component({
  properties: {
    reachBottomNum: {
      type: Number,
      value: 0,
      observer() {
        this.getNext();
      }
    },
    userInfo: {
      type: String,
      value: '{}',
      observer(newVal) {
        const userInfo = JSON.parse(wx.getStorageSync('userInfo') || '{}');
        newVal = JSON.parse(newVal || '{}');
        wx.setStorageSync('userInfo', JSON.stringify(Object.assign(userInfo, newVal)));
      }
    }
  },
  data: {
    domain,
    listData: {},
  },
  ready() {
    this.init();
  },
  methods: {
    getListData() {
      const userInfo = JSON.parse(this.data.userInfo);
      const listData = this.data.listData;
      listData.ajaxIng = true;
      this.setData({listData});
      ajax({
        url: 'user/userStore',
        data: {
          openid: userInfo.openid,
          bbs_id: listData.bbsId,
          handleType: 'next',
          range: 20,
        },
        success: ({items = []}) => {
          items.forEach((item) => item.tag = item.tag  || []);
          listData.data = listData.data.concat(items);
          listData.bbsId = items.length > 0 && items[items.length - 1].id || 0;
          listData.dataGet = items.length < 20 ? false : true;
          listData.ajaxIng = false;
          this.setData({listData});
        }
      });
    },
    getNext() {
      if (this.data.listData.ajaxIng || !this.data.listData.dataGet) return;
      this.getListData();
    },
    init() {
      this.setData({listData: {data: [], dataGet: true, ajaxIng: false, bbsId: 0}});
      this.getListData();
    },
  }
});